import logo from './logo.svg';
import './App.css';
import { Fragment } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Layout from './Components/Layout/Layout';
import Home from './Components/Home/Home';
import Offers from './Components/Offers/Offers';
import Payment from './Components/Payment/Payment';
import OfferInfo from './Components/Offers/OfferInfo';
import NotFound from './Components/NotFound/NotFound';

function App() {
  return (
    //<Layout>
      <Switch>
        <Route path='/' exact>
          <Redirect to='/Home'/>
        </Route>
        <Route path='/Home' exact>
          <Home/>
        </Route>
        <Route path='/Offers' exact>
          <Offers/>
        </Route>
        <Route path='/Payment' exact>
          <Payment/>
        </Route>
        <Route path='/OfferInfo'>
          <OfferInfo />
        </Route>
        <Route path='*' exact>
          <NotFound/>
        </Route>
      </Switch>
    //</Layout>
  );
}

export default App;
