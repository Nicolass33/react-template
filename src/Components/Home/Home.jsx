import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import Button from "@mui/material/Button";

import image from '../../Assets/images/the-road-1361700.jpg';
import './Home.css';

class Home extends Component {
    render(){
        return (
            <div className="bodyContainer">
                <img style={{ backgroundImage:`url(${image})` }} className="homeBackground">

                </img>
                <div className="textContainer">
                    <p className="welcomeText">Welcome to SIXT</p>
                    <p>Rent a car throughout the world</p>
                    <i>Because you deserve to travel with luxury</i>
                </div>
                <div className="buttonContainer">
                    <NavLink to="/Offers">
                        <Button className="button" variant="contained">Deals</Button>
                    </NavLink>
                </div>
            </div>
        )
    }
}

export default Home;