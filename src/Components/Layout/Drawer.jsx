import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import Divider from '@mui/material/Divider';
import HomeIcon from '@mui/icons-material/Home';
import LocalOfferIcon from '@mui/icons-material/LocalOffer';
import PaymentIcon from '@mui/icons-material/Payment';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Toolbar from '@mui/material/Toolbar';

import classes from './Drawer.css';

class DrawerComponent extends Component {
    getRoute(text){
        switch(text){
            case 'Home': return <HomeIcon/>
            case 'Offers': return <LocalOfferIcon/>
            case 'Payment': return <PaymentIcon/>
            default: return null;
        }
    }
    render(){
        return (
            <div>
                <Toolbar />
                <Divider />
                <List>
                    {['Home', 'Offers', 'Payment'].map((text) => (
                    <NavLink to={text} className="textNavLink" activeClassName="active">
                        <ListItem button key={text}>
                            <ListItemIcon className="icon">
                                {this.getRoute(text)}
                            </ListItemIcon>
                            <ListItemText primary={text} />
                        </ListItem>
                    </NavLink>
                    ))}
                </List>                
            </div>
        )
    }
}

export default DrawerComponent;



