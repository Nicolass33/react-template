import React, { Component } from 'react';

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';

import DrawerComponent from './Drawer';

class Layout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
      mobileOpen: true
    };
  }

  render(){
      const drawerWidth = 200;
      const { window } = this.props;

      const handleDrawerToggle = () => {
          this.setState(
            this.state.mobileOpen = !this.state.mobileOpen
          )
      };

      const container = window !== undefined ? () => window().document.body : undefined;

      return (
          <div>
              <Box sx={{ display: 'flex' }}>
                <CssBaseline />
                <AppBar position="fixed" sx={{ bgcolor: "darkorange",zIndex: (theme) => theme.zIndex.drawer + 1 }}>
                  <Toolbar>
                    <Typography variant="h6" noWrap component="div">
                      SIXT
                    </Typography>
                  </Toolbar>
                </AppBar>
                <Box
                  component="nav"
                  sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
                  aria-label="mailbox folders"
                >
                  <Drawer
                    variant="permanent"
                    sx={{
                      display: { xs: 'none', sm: 'block' },
                      '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                    }}
                    open
                  >
                    <DrawerComponent/>
                  </Drawer>
                </Box>
              </Box>
          </div>
      )
  }
}

export default Layout;