import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import Button from "@mui/material/Button";

import './NotFound.css';

class NotFound extends Component {
    render(){
        return (
            <div className="bodyContainer">
                <div className="textContainer">
                    <p className="textNotFound">404 Page not found</p>
                </div>
                <div className="buttonContainer">
                    <NavLink to="/Home">
                        <Button className="button" variant="contained">Go Home</Button>
                    </NavLink>
                </div>
            </div>
        )
    }
}

export default NotFound;