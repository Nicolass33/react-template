import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

import Button from "@mui/material/Button";
import CircularProgress from '@mui/material/CircularProgress'

import car from '../../Assets/images/car-exterior-1480568.jpg';
import './OfferInfo.css';

class OfferInfo extends Component {
    constructor(props){
        super(props);
        this.state = {
            rows: [
                {name: 'Frozen yoghurt', calories: 159, fat: 6.0, carbs: 24, protein: 4.0},
                {name: 'Ice cream sandwich', calories: 237, fat: 9.0, carbs: 37, protein: 4.3},
                {name: 'Eclair', calories: 262, fat: 6.0, carbs: 24, protein: 6.0},
                {name: 'Cupcake', calories: 305, fat: 3.7, carbs: 67, protein: 4.3},
                {name: 'Gingerbread', calories: 56, fat: 6.0, carbs: 49, protein: 3.9},
            ]
        }
    }


    render(){
        console.log("props = ", this.props)
        if(this.props.offer){
            return (
                <div className="bodyContainer">
                    <div className="OfferInfoDescription">
                        {this.props.offer.description}
                    </div>
                    <div className="offerInfoContainer">
                        <div className="offerImage">
                            <img className="imageSlider imageInfo" src={car}></img>
                        </div>
                        <div className="offerInfo">
                            <div className="infoTableRow">
                                <b className="title">Body style: </b>
                                <span className="info">{this.props.offer.carGroupInfo.bodyStyle}</span>
                            </div>
                            <hr className="brLine"/>
                            <div className="infoTableRow">
                                <b className="title">Car: </b>
                                <span className="info">{this.props.offer.headlines.description}</span>
                            </div>
                            <hr className="brLine"/>
                            <div className="infoTableRow">
                                <b className="title">Mileage included: </b>
                                <span className="info">{this.props.offer.mileageInfo.display}</span>
                            </div>
                            <hr className="brLine"/>
                            <div className="infoTableRow">
                                <b className="title">Rental days: </b>
                                <span className="info">{this.props.offer.rentalDays}</span>
                            </div>
                            <hr className="brLine"/>
                            <div className="infoTableRow">
                                <b className="title">Status: </b>
                                <span className="info">{this.props.offer.status}</span>
                            </div>
                            <hr className="brLine"/>
                        </div>
                    </div>
                    <div className="buttonContainer">
                        <NavLink to="/Offers">
                            <Button className="button buttonOfferInfo" variant="contained">Go Back</Button>
                        </NavLink>
                        <NavLink to="/Payment">
                            <Button className="button buttonOfferInfo" variant="contained">Rent</Button>
                        </NavLink>
                    </div>
                </div>
            )
        } else {
            return(
                <div className="bodyContainer">
                    <CircularProgress />
                </div>
                
            )
        }
    }
}

const mapStateToProps = function(state) {
    console.log("state = ", state)
    return {
        offer: state.currentOffer,
    }
}

export default connect(mapStateToProps)(OfferInfo);