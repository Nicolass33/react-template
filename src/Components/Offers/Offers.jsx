import React, { Component } from "react";
import { connect } from 'react-redux';
import { getOffers } from '../../Store/Actions/action';

import CircularProgress from '@mui/material/CircularProgress'

import Slider from './Slider';

import './Offers.css';

class Offers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            offers: [],
            currentPage: 0,
            size: 9
        };
        
    }

    componentDidMount(){
        this.props.getOffers();
    }

    render(){
        console.log("current props =", this.props)
        if(this.props.offers){
            return (
                <div className="bodyContainer">
                    <p className="offerText">Car Rental Offers</p>
                    <div className="gridContainer">
                        {this.props.offers.map((offer, index) => (
                            (index < this.state.size) ? <Slider key={index} offer = {offer}/> : null
                        ))}
                    </div>
                </div>
            )
        } else {
            return(
                <CircularProgress />
            )
        }
        
    }
}

const mapStateToProps = function(state) {
    return {
        offers: state.offers,
    }
}

export default connect(mapStateToProps, {getOffers})(Offers);