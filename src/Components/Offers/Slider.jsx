import React, { Component } from 'react';

import { Redirect } from 'react-router';
import { connect } from 'react-redux';
import { setCurrentOffer } from '../../Store/Actions/action';

import car from '../../Assets/images/car-exterior-1480568.jpg';
import './Slider.css';

class Slider extends Component {
    constructor(props){
        super(props)
        this.state = {
            offer: props.offer,
            offerInfoOpen: true
        }
        this.showOfferInfo = this.showOfferInfo.bind(this);
    }

    setOffer() {
        this.props.setCurrentOffer(this.props.offer);
    }

    showOfferInfo() {
        this.setOffer();
        this.setState({
            offerInfoOpen: !this.state.offerInfoOpen
        })
    }

    render(){
        if(this.state.offerInfoOpen){
            return (
                <div className="sliderContainer" onClick={this.showOfferInfo}>
                    <div className="sliderImage">
                        <img className="imageSlider" src={car}></img>
                    </div>
                    <div className="sliderTextContainer">
                        <p className="textOffer">{this.props.offer.description}</p>
                        <p className="priceOffer">{this.props.offer.prices.basePrice.amount.value} {this.props.offer.prices.basePrice.amount.currency}</p>
                    </div>
                </div>
            )
        } else {
            return(
                <Redirect to={{
                    pathname: '/OfferInfo'
                }}/>
            )
            
        }
        
    }
}

export default connect(null, {setCurrentOffer})(Slider);