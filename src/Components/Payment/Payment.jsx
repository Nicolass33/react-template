import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import TextField from '@mui/material/TextField';
import Button from "@mui/material/Button";

import './Payment.css';

class Payment extends Component {
    render(){
        return (
            <div className="bodyContainer">
                <div className="paymentText">
                    Payment Method
                </div>
                <div className="formValues">
                    <div className="completeRow">
                        <TextField
                            id="outlined-error"
                            label="Name on Card"
                            defaultValue="Name on Card"
                            className="inputForm"
                        />
                    </div>
                    <div className="completeRow">
                        <TextField
                            id="outlined-error"
                            label="Card number"
                            defaultValue="Card number"
                            className="inputForm"
                        />
                    </div>
                    <div className="halfRow">
                        <TextField
                            id="outlined-error"
                            label="date"
                            defaultValue="MM/YY"
                            className="inputForm"
                        />
                        <TextField
                            id="code"
                            label="code"
                            defaultValue="code"
                            className="inputForm"
                        />
                    </div>
                    <div className="buttonContainer">
                        <NavLink to="/Home">
                            <Button className="button" variant="contained">Pay now</Button>
                        </NavLink>
                    </div>
                </div>
            </div>
        )
    }
}

export default Payment;