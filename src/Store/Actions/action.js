import { GETOFFERS, OFFERSERROR, GETCURRENTOFFER } from '../../Constants/Constants';

import axios from 'axios';

export const getOffers = () => async dispatch => {
    try{
        const response = await axios.get('http://cdn.sixt.io/codingtask/offers.json');
        dispatch({
            type: GETOFFERS,
            payload: response.data.offers
        })
    }catch(error){
        dispatch({
            type: OFFERSERROR,
            payload: error
        })
    }
}

export const setCurrentOffer = (offer) => async dispatch => {
    try{
        dispatch({
            type: GETCURRENTOFFER,
            payload: offer
        })
    }catch(error){
        dispatch({
            type: OFFERSERROR,
            payload: error
        })
    }
}