import { combineReducers } from 'redux'
import offerReducer from './reducer';

export default combineReducers({
  offers: offerReducer
})