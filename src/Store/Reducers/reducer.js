import { createSrote } from 'redux';
import * as constants from '../../Constants/Constants';

const initialState = {
    offers: [],
    currentOffer: null
}

export default function offerReducer(state = initialState, action) {
    switch(action.type){
        case constants.GETOFFERS: {
            return {
                ...state,
                offers: action.payload
            };
        }

        case constants.GETCURRENTOFFER :{
            return {
                ...state,
                currentOffer: action.payload
            }
        }

        case constants.OFFERSERROR: {
            return{
                ...state,
                error: action.payload
            }
        }

        default: 
            return state
    }
}