import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore } from "redux";
import Layout from './Components/Layout/Layout';

import './index.css';
import App from './App';
import store from './Store/store';


ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
        <Layout/>
        <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);

